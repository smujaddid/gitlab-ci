# Overview

This is a central repository for Gitlab CI configuration for various projects.

# Usage

Include your preferred configuration such as:

```yml
include:
  - project: 'smujaddid/gitlab-ci'
    file: '/npm.gitlab-ci.yml'

variables:
  RUN_NPM_BUILD: 0

```

# NPM

You have to include `/npm.gitlab-ci.yml` into your `gitlab-ci.yml` file

```yml
include:
  - project: 'smujaddid/gitlab-ci'
    file: '/npm.gitlab-ci.yml'

variables:
  RUN_NPM_BUILD: 0

```
